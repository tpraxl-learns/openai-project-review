import fs from "fs";
import path from "path";

function readFileContent(absoluteProjectFolder, fileName) {
    return fs.readFileSync(path.join(absoluteProjectFolder, fileName), {
        encoding: 'utf-8'
    })
}

function readFileContents(absoluteProjectFolder, fileNames) {
    return fileNames.map(fileName => ({
        fileName: fileName,
        content: readFileContent(absoluteProjectFolder, fileName)
    }))
}

export function getReadFileContentFunctions(_projectInformation) {
    return {
        readFileContents: {
            implementation: readFileContents,
            description: {
                type: "function",
                function: {
                    name: "readFileContents",
                    description: "Read the content of multiple files to review individual files and get content insight. The fileNames are relative to the projectFolder.",
                    parameters: {
                        type: "object",
                        properties: {
                            projectFolder: {
                                type: "string"
                            },
                            fileNames: {
                                type: "array",
                                items: {
                                    type: "string"
                                }
                            }
                        },
                        required: ["projectFolder", "fileNames"]
                    }
                }
            }
        },
    }
}