import {execSync} from "child_process";

export function proposeCommandForDependencies(allProjectFiles) {
    return allProjectFiles.includes('package.json') ? getNpmProjectDependencies : false
}

export function getProjectDependencyFunctions(projectInformation) {
    if(!projectInformation.commandForDependencies) {
        return {}
    }
    return {
        getProjectDependencies: {
            implementation: projectInformation.commandForDependencies,
            description: {
                type: "function",
                function: {
                    name: "getProjectDependencies",
                    description: "Returns the packages/ libraries the code depends on. Helps to understand the project's working, as it tells about the technologies, the project is built with.",
                    parameters: {
                        type: "object",
                        properties: {
                            projectFolder: {
                                type: "string"
                            },
                        },
                        required: ["projectFolder"]
                    }
                }
            }
        },
    }
}

function getNpmProjectDependencies(projectFolder) {
    process.chdir(projectFolder)
    return execSync('npm ls --all', {  encoding: 'utf-8'})
}