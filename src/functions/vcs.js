import {execSync} from "child_process";

export function getGitFunctions(projectInformation) {
    if(!projectInformation.isGitProject) {
        return {}
    }
    return {
        getCommitHistory: {
            implementation: getCommitHistory,
            description: {
                type: "function",
                function: {
                    name: "getCommitHistory",
                    description: "Get commit history of the project. Useful for understanding the changes made to the project over time.",
                    parameters: {
                        type: "object",
                        properties: {
                            projectFolder: {
                                type: "string"
                            }
                        },
                        required: ["projectFolder"]
                    }
                }
            }
        },
    }
}

function getCommitHistory(absoluteProjectFolder) {
    process.chdir(absoluteProjectFolder)
    return execSync('git log --stat', {  encoding: 'utf-8'})
}