import OpenAI from 'openai';
import {sleep} from "openai/core";

// export OPENAI_API_KEY
const openai = new OpenAI();

const defaultConfigObject = {
    instructions: [{role: "system", content: ''}],
    userInput: "",
    iterationLimit: 5,
    availableTools: {}
}
export async function askOpenAi(configObject) {
    const { instructions, userInput, iterationLimit, availableTools} = {...defaultConfigObject, ...configObject}
    if (iterationLimit < 1) {
        throw new Error('iteration limit must be positive > 0')
    }
    const messages = [
        ...instructions,
        { role: "user", content: userInput }
    ]
    for (let i = 0; i < iterationLimit; i++) {
        console.info(`iterating ${i}`)
        const response = await openai.chat.completions.create({
            model: "gpt-4-turbo-preview",
            messages: messages,
            tools: Object.values(availableTools).map(tool => tool.description),
        });
        const { finish_reason, message } = response.choices[0];

        if (finish_reason === "tool_calls" && message.tool_calls) {
            const functionName = message.tool_calls[0].function.name;
            const functionToCall = availableTools[functionName]?.implementation;
            if (!functionToCall) {
                throw new Error(`${functionName} could not be resolved`)
            }
            const functionArgs = JSON.parse(message.tool_calls[0].function.arguments);
            const functionArgsArr = Object.values(functionArgs);
            console.log("calling", {functionName, functionArgsArr})
            const functionResponse = await functionToCall.apply(null, functionArgsArr);
            console.dir(functionResponse, { depth: null, maxArrayLength: null })
            messages.push({
                role: "function",
                name: functionName,
                content: `
          The result of the last function was this: ${JSON.stringify(
                    functionResponse
                )}
          `,
            });
        } else if (finish_reason === "stop") {
            messages.push(message);
            return message.content;
        }
        await sleep(8)
    }
    return "The maximum number of iterations has been met without a suitable answer. Please try again with a more specific input.";
}