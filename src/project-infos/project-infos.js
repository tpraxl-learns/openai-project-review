import fs from "fs";
import path from "path";
import {proposeCommandForDependencies} from "../functions/dependencies.js";

export function getProjectInformation(absoluteFolderPath) {
    const allFiles = getAllFiles(absoluteFolderPath)
    return {
        name: "first-test",
        projectFolder: absoluteFolderPath,
        files: getAllFiles(absoluteFolderPath).map(path => path.substring(absoluteFolderPath.length + 1)).filter(path => !path.startsWith('.idea/') && !path.startsWith('.git/') && !path.startsWith('node_modules')),
        isGitProject: allFiles.some(file => file.startsWith('.git/')),
        commandForDependencies: proposeCommandForDependencies(allFiles)
    }
}

function getAllFiles(dirPath, arrayOfFiles) {
    const files = fs.readdirSync(dirPath)

    arrayOfFiles = arrayOfFiles || []

    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
        } else {
            arrayOfFiles.push(path.join(dirPath, "/", file))
        }
    })

    return arrayOfFiles
}