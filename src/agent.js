import {askOpenAi} from "./core/ask-openai.js";
import {getProjectInformation} from "./project-infos/project-infos.js";
import {getProjectDependencyFunctions} from "./functions/dependencies.js";
import {getReadFileContentFunctions} from "./functions/read-file-content.js";
import {getGitFunctions} from "./functions/vcs.js";

export async function agent(folderPath) {
    const projectInformation = getProjectInformation(folderPath)
    return await askOpenAi({
        instructions: provideInstructions(projectInformation),
        userInput: "Please review my project.",
        iterationLimit: 10,
        availableTools: provideAvailableTools(projectInformation)
    })
}

function provideInstructions(projectInformation) {
    return [
        {
            role: "system",
            content: `
            You are an experienced programmer and a consultant for better code.
            When you are asked to review a project, use the functions you have been provided with
            to read the code, get content insights and access project information.
        
            Follow these steps:
            
            1. Describe your approach to the review
            2. Enumerate all essential project files and print the essential files as part of your answer.
            3. Gain a complete understanding of the project's functionality and architecture. Read the file contents of all essential files.
            4. Get the project dependencies if this supports the review
            5. Get the commit history if this helps supports review
            6. Aim to provide a thorough, complete review. Do not propose further steps that you could have taken on your own.
            7. Provide top 5 improvement suggestions with concrete code examples based on the reviewed code
            
            Do not ask the user if you should continue to review. Instead of proposing next steps, prefer to execute them directly given the functions you are provided with.
            Propose next steps only if you cannot execute these steps by yourself.
        
            It is essential to conclude your review with information about OpenAIs usage of the information gathered in this chat: Can the owner of the reviewed project be sure that you do not use the content of her source files?`
        },
        {
            role: "function",
            name: "getProjectInformation",
            content: `The result of the last function was this: ${JSON.stringify(projectInformation)}`
        }
    ]
}

function provideAvailableTools(projectInformation) {
    return {
        ...getReadFileContentFunctions(projectInformation),
        ...getGitFunctions(projectInformation),
        ...getProjectDependencyFunctions(projectInformation)
    }
}
