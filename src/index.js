import {agent} from "./agent.js";

async function review(args) {
    if (args.length !== 1) {
        throw new Error("Provide one folder path")
    }
    const folderPath = args[0]
    const response = await agent(folderPath)
    console.log(response)
}

const args = process.argv.slice(2);
await review(args)
